package uk.ac.reading.ruby.buildingconsole;

import java.awt.Point;
import java.util.ArrayList;

public class Person
{
    private Point P, finalDest; //location
    private ArrayList<Point> pDests; //destination points for body to follow

    Person(Point L)
    {
        P = L;
    }

    public void setPosition(Point L)
    {
        P = L;
    }

    public Point getPos()
    {
        return P;
    }

    /**
     * show person in the given building interface
     * @param bi
     */
    public void showPerson(BuildingInterface bi)
    {
        // call the showIt method in bi
        bi.showIt(P.x + 1, P.y + 1,'P');
    }

    public void addDest(Point p)
    {
        pDests.add(p);
    }

    public void clearDest()
    {
        pDests = new ArrayList<Point>();
        pDests.clear();
    }

    //initialises the moving
    public boolean beginMoving()
    {
        finalDest = pDests.get(pDests.size() - 1);
        Point nextDest;
        int dct = 0;
        do {
            nextDest = pDests.get(dct);
            dct++;
        } while (P.equals(nextDest));

        movePerson(nextDest);

        if (P.equals(finalDest))
            return false;
        else return true;
    }

    //moves the person to the point given
    public void movePerson(Point D)
    {
        int dx = D.x - P.x, dy = D.y - P.y;

        if (dx > 0)
            dx = 1;
        if (dx < 0)
            dx = -1;
        if (dy > 0)
            dy = 1;
        if (dy < 0)
            dy = -1;
        //Makes sure it only moves one space in each direction

        P.translate(dx, dy);
    }

    //returns string with point of person
    public String toString()
    {
        String personText = "Person at " + P.x + ", " + P.y;
        return personText;
    }
}
