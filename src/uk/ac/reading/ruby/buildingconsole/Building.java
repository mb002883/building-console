package uk.ac.reading.ruby.buildingconsole;

import java.util.ArrayList;
import java.awt.Point;
import java.util.Random;

public class Building
{
	private
		int xSize, ySize; //
	private
		ArrayList<Room> allRooms; //array of all rooms in the building
	private
		Person body; //person in building
	private
		Point bodyPos; //position of person
		Random randGen;
	private
		String createS; //original input string
	
	Building(String S)
	{
		setBuilding(S);
	}

	public void setBuilding(String bS)
	{
		createS = bS;
		allRooms = new ArrayList<Room>();
		Random randGen; //initialises random generator
		randGen = new Random();
		String[] buildingStrings = bS.split(";");
		String[] buildingSizeString = buildingStrings[0].split(" ");
		xSize = Integer.valueOf(buildingSizeString[0]);
		ySize = Integer.valueOf(buildingSizeString[1]);

		for (int i = 1; i < buildingStrings.length; i++)
		{
			allRooms.add(new Room(buildingStrings[i]));
		}

		int randRoomIndex = randGen.nextInt(allRooms.size()); //gets random room in building
		bodyPos = new Point(allRooms.get(randRoomIndex).getRandomPointInRoom(randGen)); //sets position of person
		body = new Person(bodyPos);

        body.clearDest();
	}

	public int getxSize()
	{
		return xSize;
	}
	
	public int getySize()
	{
		return ySize;
	}
	
	public String getCreateString()
	{
		return createS;
	}
	
	//returns a random room in the building
	private Room getRdmRoom()
	{
		randGen = new Random();
		int randRoomIndex = randGen.nextInt(allRooms.size()); //gets random room in building
		Room random = new Room("0 0 5 5 0 2");
		random = allRooms.get(randRoomIndex);
		return random;
	}
	
	//returns room number of person and -1 if they are not in a room
	private int checkRoomOfPerson(Person body)
	{
		int roomNum = -1;
		for (int i = 0; i < allRooms.size(); i++)
		{
			if (allRooms.get(i).isInRoom(body.getPos())) //not  in room
				roomNum = i;
		}
		return roomNum;
	}

	//returns string describing the buidling
	public String toString()
	{
		String buildingText = "Building size: " + xSize + ", " + ySize + "\n";
		for (Room r : allRooms)
		{
			buildingText += r.toString() + "\n";
		}

		buildingText += body.toString() + "\n";

		int bodyRoom = checkRoomOfPerson(body);
		buildingText += "The person is in " + allRooms.get(bodyRoom).toString() + "\n";

		return buildingText;
	}

	/**
	 * show all building's rooms and occupants in the interface
	 * @param bi	the interface
	 */
	public void showBuilding (BuildingInterface bi)
	{
		for (Room r : allRooms)
		{
			r.showRoom(bi);
		}

		body.showPerson(bi);
	}
	
	public void initMove()
	{
		body.clearDest(); //clears destination array
		
		Room bodyRoom, nextRoom = new Room("0 0 5 5 0 2");
		if (this.checkRoomOfPerson(body) != -1) //if person is in room
    	{
    		bodyRoom = allRooms.get(this.checkRoomOfPerson(body));
    		body.addDest(bodyRoom.getDoorPoint(-1)); //adds points just inside and just outside of the current room
    		body.addDest(bodyRoom.getDoorPoint(1));
    	}
		
    	nextRoom = this.getRdmRoom();
    	body.addDest(nextRoom.getDoorPoint(1)); //adds points for the next room
		body.addDest(nextRoom.getDoorPoint(-1));
	}

    public boolean moveBody()
    {
    	boolean moving;
        moving = body.beginMoving(); //calls the mov function for the body
        return moving;
	}

	public static void main(String[] args) 
	{
		// testing
		Building b = new Building("11 11;0 0 5 5 3 5;6 0 10 10 6 6;0 5 5 10 2 5");
		System.out.println(b.toString());
	}
}
